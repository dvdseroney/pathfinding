The maze is defined in get_maze()

The '#' sign depicts a barrier.
Any other character is part of a valid path.
The start point is the uppercase letter X.
The end point is the uppercase letter O.

Change the rows as needed in get_maze.
Each additional row should follow the same naming convention (row5, row6, row7...)
Rows can vary in length.