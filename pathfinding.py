import queue

def get_maze():

    return {
        'row0': ['', '', '', '', '', ''],
        'row1': ['', '#', '#', '#', '#', 'O'],
        'row2': [''],
        'row3': ['', '#', '', '#', 'X'],
        'row4': ['', '', '', '', ''],
        }

def get_start_point(maze):
    print('Looking for start point')
    for row, row_key in enumerate(maze):
        for index, value in enumerate(maze[row_key]):
            if value == 'X':
                print('found starting point: {index}, {row}'.format(index=index, row=row))
                return [index, row]
            print('checking next index')
        print('checking next row')
    print('nothing found')
    return

def valid_movement(movements, start_point_coordinates, maze):
    vectors = {
        'U': -1,
        'D': 1,
        'L': -1,
        'R': 1
    }

    start_row_index, start_row = start_point_coordinates

    start_row_key = 'row' + str(start_row)

    if start_row_index < 0 or start_row_key not in maze or len(maze[start_row_key]) <= start_row_index or maze[start_row_key][start_row_index] == '#':
        print('invalid start point!')
        return

    current_index, current_row = start_point_coordinates

    for current_movement_index, movement in enumerate(movements):
        if movement in ['U', 'D']:
            current_row += vectors[movement]
            if 'row' + str(current_row) not in maze or current_index >= len(maze['row' + str(current_row)]):
                return False, [current_index, current_row]
            if current_movement_index != 0:
                if((movement == 'U' and movements[current_movement_index - 1] == 'D') or (movement == 'D' and movements[current_movement_index - 1] == 'U')):
                    return False, [current_index, current_row]

        elif movement in ['L', 'R']:
            current_index += vectors[movement]
            if current_index < 0 or current_index >= len(maze['row' + str(current_row)]):
                return False, [current_index, current_row]
            if current_movement_index != 0:
                if((movement == 'L' and movements[current_movement_index - 1] == 'R') or (movement == 'R' and movements[current_movement_index - 1] == 'L')):
                    return False, [current_index, current_row]

        if maze['row' + str(current_row)][current_index] == '#':
            return False, [current_index, current_row]
    
    return True, [current_index, current_row]

path_found = False
maze_impossible = False
maze = get_maze()
starting_point = get_start_point(maze)
vectors = ['U', 'D', 'L', 'R']
paths = queue.Queue()
paths.put('')

if not starting_point:
    print('Invalid Starting point!')
    exit()
            
while not path_found and not maze_impossible:
    next_path = paths.get()
    for vector in vectors:
        new_path = next_path + vector
        path_is_valid, [new_index, new_row] = valid_movement(new_path, starting_point, maze)
        if path_is_valid:
            paths.put(new_path)
            if maze['row' + str(new_row)][new_index] == 'O':
                path_found = True
                print('PATH is %s' % new_path)
                break
                
    maze_impossible = paths.qsize() == 0

if maze_impossible:
    print('Maze cannot be solved')